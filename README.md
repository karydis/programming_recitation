# Intro to Programming

### Agenda

* Computer architecture, memory, compile vs interpret, JIT

* Programming 101
    - Python Hello World, [Python Standard Libraries](https://docs.python.org/3/library/index.html)
    - Variables, Types, etc
    - Conditionals ("if/else") 
    - Iterations ("for loops")
    - Functions, Scopes, [Arduino Functions](https://www.arduino.cc/en/Reference/FunctionDeclaration)
    - Objects
    - Writing readable, clean, sharable code
    
* Programming Languages
    - low level -> abstract
    - C Hello World, [C Standard Libraries](https://en.wikipedia.org/wiki/C_standard_library)
        ```
            gcc -o hello_world hello_world.c
            ./hello_world
        ```
    - Javascript Hello World
        ```
            javac HelloWorld.java
            java HelloWorld
        ```
    - NodeJS Hello World
        ```
            node hello_world_node.js
            # and then visit in a browser http://localhost:8081
        ```
    - Debugging/Testing

* Programming Embedded Systems
    - Embedded C, [Arduino Language Reference](https://www.arduino.cc/en/Reference/HomePage)
    - Memory: EEPROM, Registers
    - Arduino Hello world

### Resources

* [Arduino Foundations](https://www.arduino.cc/en/Tutorial/Foundations)
* [C Programming Tutorial](https://www.cprogramming.com/tutorial/c-tutorial.html)
* [Python Programming Tutorial](https://pythonprogramming.net/introduction-to-python-programming/)
* [JavaScript Programming Tutorial (and more)](https://www.khanacademy.org/computing/computer-programming)
* [NodeJS Progamming Tutorial](https://www.tutorialspoint.com/nodejs/nodejs_first_application.htm)
* [Jupyter Notebooks](http://jupyter.org/)